Rails.application.routes.draw do
  devise_for :sign_ups
  devise_for :users
  resources :microposts
  resources :users
  root 'users#index'
end
